<?php

namespace addons\channel;

use app\common\library\Menu;
use think\Addons;

/**
 * 插件
 */
class Channel extends Addons
{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        $menu = [
            [
                'name'    => 'channel',
                'title'   => '渠道管理',
                'icon'    => 'fa fa-feed',
                'sublist' => [
                    [
                        'name' => 'channel/index',
                        'title' => '渠道列表',
                        'icon' => 'fa fa-circle-o',
                        'sublist' => [
                            ['name' => 'channel/index/index', 'title' => '查看'],
                            ['name' => 'channel/index/add', 'title' => '添加'],
                            ['name' => 'channel/index/edit', 'title' => '编辑'],
                            ['name' => 'channel/index/del', 'title' => '删除'],
                        ]
                    ],
                    [
                        'name' => 'channel/bank',
                        'title' => '提现方式',
                        'icon' => 'fa fa-circle-o',
                        'sublist' => [
                            ['name' => 'channel/bank/index', 'title' => '查看'],
                            ['name' => 'channel/bank/add', 'title' => '添加'],
                            ['name' => 'channel/bank/edit', 'title' => '编辑'],
                            ['name' => 'channel/bank/del', 'title' => '删除'],
                        ]
                    ],
                    [
                        'name' => 'channel/withdraw',
                        'title' => '提现记录',
                        'icon' => 'fa fa-circle-o',
                        'sublist' => [
                            ['name' => 'channel/withdraw/index', 'title' => '查看'],
                            ['name' => 'channel/withdraw/edit', 'title' => '编辑'],
                            ['name' => 'channel/withdraw/del', 'title' => '删除'],
                            ['name' => 'channel/withdraw/withdraw', 'title' => '提现申请'],
                        ]
                    ],
                    [
                        'name' => 'channel/moneylog',
                        'title' => '余额明细',
                        'icon' => 'fa fa-circle-o',
                        'sublist' => [
                            ['name' => 'channel/moneylog/index', 'title' => '查看'],
                        ]
                    ],
                    [
                        'name' => 'channel/commission',
                        'title' => '佣金明细',
                        'icon' => 'fa fa-circle-o',
                        'sublist' => [
                            ['name' => 'channel/commission/index', 'title' => '查看'],
                        ]
                    ],
                    [
                        'name' => 'channel/poster',
                        'title' => '推广海报',
                        'icon' => 'fa fa-circle-o',
                        'sublist' => [
                            ['name' => 'channel/poster/index', 'title' => '查看'],
                            ['name' => 'channel/poster/add', 'title' => '添加'],
                            ['name' => 'channel/poster/edit', 'title' => '编辑'],
                            ['name' => 'channel/poster/del', 'title' => '删除'],
                            ['name' => 'channel/poster/copyurl', 'title' => '复制链接'],
                            ['name' => 'channel/poster/downloadqrcode', 'title' => '下载二维码'],
                            ['name' => 'channel/poster/downloadposter', 'title' => '下载海报'],
                        ]
                    ],
                    [
                        'name' => 'channel/user',
                        'title' => '用户列表',
                        'icon' => 'fa fa-circle-o',
                        'sublist' => [
                            ['name' => 'channel/user/index', 'title' => '查看'],
                        ]
                    ],
                    
                ]
            ]
        ];
        Menu::create($menu);
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        Menu::delete('channel');
        return true;
    }

    /**
     * 插件启用方法
     * @return bool
     */
    public function enable()
    {
        Menu::enable('channel');
        return true;
    }

    /**
     * 插件禁用方法
     * @return bool
     */
    public function disable()
    {
        Menu::disable('channel');
        return true;
    }

    /**
     * 用户注册完成钩子
     * 
     * 触发时机: 当用户成功注册, 并由程序写入到用户表中之后
     * 
     * 用户完成注册时, 如果是通过渠道的推广码进入的, 则需要将此用户列入到该渠道名下
     *
     * @return void
     * @author AriFe.Liu 
     */
    public function userRegister($params){
        # 在这里做一些事情
        # channel_user 表写入渠道与用户的关联信息     
        $channel_id = $params['channel_id'];
        $channel_info = db('Channel')->where(['id'=>$channel_id])->find();
        $user_id = $params['user_id'];
        $res = db('ChannelUser')->where([
            'channel_id' => $channel_id,
            'user_id' => $user_id
        ])->find();
        if(!$res){
            $res = db('ChannelUser')->insert([
                'channel_id' => $channel_id,
                'user_id' => $user_id,
                'createtime' => time(),
                'admin_id' => $channel_info['admin_id'],
            ]);
            if(!$res){
                $params['msg'] = '插入渠道-用户关联关系失败';
                // logs($params);
                return $params;
            }
        }
        return true;
    }

}

<?php

return [
    [
        'name' => 'channel_role_id',
        'title' => '渠道角色ID',
        'type' => 'number',
        'tip' => '请进入管理员角色管理页新建或查看',
        'value' => '2',
        'extend' => '',
        'rule' => '',
    ],
    [
        'name' => 'superloginhost',
        'title' => '渠道登录域名',
        'type' => 'string',
        'tip' => '渠道登录专用域名,如未配置,则与当前页域名一致(必须完整带http)',
        'value' => '',
        'extend' => '',
        'rule' => '',
    ],
    [
        'name' => 'relmodule',
        'title' => '关联模型',
        'type' => 'array',
        'tip' => '暂用于渠道佣金表的多态关联',
        'value' => [
            'order' => 'app\\admin\\model\\Order',
        ],
        'extend' => '',
        'rule' => '',
    ],
];

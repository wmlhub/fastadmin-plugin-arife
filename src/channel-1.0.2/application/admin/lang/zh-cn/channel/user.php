<?php

return [
    'Id'               => 'ID',
    'Channel_id'       => '渠道ID',
    'User_id'          => '用户ID',
    'Createtime'       => '创建时间',
    'Channel.truename' => '真实姓名',
    'User.nickname'    => '昵称',
    'User.avatar'      => '头像'
];
